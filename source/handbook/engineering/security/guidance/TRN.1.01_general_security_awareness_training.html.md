---
layout: markdown_page
title: "TRN.1.01 - General Security Awareness Training Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# TRN.1.01 - General Security Awareness Training

## Control Statement

All GitLabbers complete security awareness training, which includes updates about relevant policies and how to report security events to the authorized response team. Records of training completion are documented and retained for tracking purposes.

## Context

At GitLab we use [KnowBe4](https://www.knowbe4.com/) for our security awareness training. In the past, security awareness training has only been a task associated with on-boarding as a new Gitlabber. As we adopt new compliance frameworks and work towards an audit certification of some of these frameworks, we will need to increase the cadence of this training. Security awareness training can be seen as a hassle and disruption from normal work, but these trainings can have real value not only to GitLab as a company, but also to GitLabbers in their personal lives. Taking the time to go through this training will ensure you are up to date on security best practices which will help to minimize your risk as an individual and GitLab's risk as a company.

* Because this field changes so quickly, and because of our changing needs for compliance as a company, *we now require all GitLabbers to complete this training at least every 12 months*.

## Scope

This control applies to all GitLabbers.

## Ownership

* The GitLab compliance team is responsible for validating that every GitLaber has completed training in the last year.
* All GitLabbers are responsible for completing their security awareness training.

## Implementation Guidance

For detailed implementation guidance relevant to GitLabbers, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/TRN.1.01_general_security_awareness_training.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/TRN.1.01_general_security_awareness_training.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/TRN.1.01_general_security_awareness_training.md).

## Framework Mapping

* ISO
  * A.16.1.2
  * A.16.1.3
  * A.7.2.1
  * A.7.2.2
* SOC2 CC
  * CC1.1
  * CC1.4
  * CC1.5
  * CC2.2
  * CC2.3
* PCI
  * 12.6
  * 12.6.1
  * 12.6.2
